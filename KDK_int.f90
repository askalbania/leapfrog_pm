program pm_KDK
   !Program calkujacy potencjal masy punktowej metoda leapfrog KDK przy zmiennym kroku calkowania
   !uzywajac interpolacji potencjalu na siatce

   !Autor: A. Skalbania, kwiecien 2014



   implicit none

   interface
      function df_dx(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell)
         integer,dimension(1, 3),intent(in) :: cell
         integer,intent(in) :: nx_cell, ny_cell, nz_cell
         !integer :: i, j, k
         real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
         real(kind=8),intent(in) :: dx_cell
         real(kind=8) :: df_dx
      end function df_dx

      function d2f_dx2(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell)
         integer,dimension(1, 3),intent(in) :: cell
         integer,intent(in) :: nx_cell, ny_cell, nz_cell
         !integer :: i, j, k
         real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
         real(kind=8),intent(in) :: dx_cell
         real(kind=8) :: d2f_dx2
      end function d2f_dx2

      function df_dy(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell)
         integer,dimension(1, 3),intent(in) :: cell
         integer,intent(in) :: nx_cell, ny_cell, nz_cell
         !integer :: i, j, k
         real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
         real(kind=8),intent(in) :: dy_cell
         real(kind=8) :: df_dy
      end function df_dy

      function d2f_dy2(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell)
         integer,dimension(1, 3),intent(in) :: cell
         integer,intent(in) :: nx_cell, ny_cell, nz_cell
         !integer :: i, j, k
         real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
         real(kind=8),intent(in) :: dy_cell
         real(kind=8) :: d2f_dy2
      end function d2f_dy2

      function df_dz(cell, pot, nx_cell, ny_cell, nz_cell, dz_cell)
         integer,dimension(1, 3),intent(in) :: cell
         integer,intent(in) :: nx_cell, ny_cell, nz_cell
         !integer :: i, j, k
         real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
         real(kind=8),intent(in) :: dz_cell
         real(kind=8) :: df_dz
      end function df_dz

      function d2f_dz2(cell, pot, nx_cell, ny_cell, nz_cell, dz_cell)
         integer,dimension(1, 3),intent(in) :: cell
         integer,intent(in) :: nx_cell, ny_cell, nz_cell
         !integer :: i, j, k
         real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
         real(kind=8),intent(in) :: dz_cell
         real(kind=8) :: d2f_dz2
      end function d2f_dz2

      function d2f_dxdy(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dy_cell)
         integer,dimension(1, 3),intent(in) :: cell
         integer,intent(in) :: nx_cell, ny_cell, nz_cell
         !integer :: i, j, k
         real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
         real(kind=8),intent(in) :: dx_cell, dy_cell
         real(kind=8) :: d2f_dxdy
      end function d2f_dxdy

      function d2f_dxdz(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dz_cell)
         integer,dimension(1, 3),intent(in) :: cell
         integer,intent(in) :: nx_cell, ny_cell, nz_cell
         !integer :: i, j, k
         real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
         real(kind=8),intent(in) :: dx_cell, dz_cell
         real(kind=8) :: d2f_dxdz
      end function d2f_dxdz

      function d2f_dydz(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell, dz_cell)
         integer,dimension(1, 3),intent(in) :: cell
         integer,intent(in) :: nx_cell, ny_cell, nz_cell
         !integer :: i, j, k
         real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
         real(kind=8),intent(in) :: dy_cell, dz_cell
         real(kind=8) :: d2f_dydz
      end function d2f_dydz
      
   end interface

      !Zmienne
      integer :: n, i, j, k, orbita, nx_cell, ny_cell, nz_cell, order
      !zmienne 'dynamiczne'
      real(kind=8),parameter :: d=1.0e-6, G=1.0, M=1.0, mp=1.0, pi=3.14159265358979

      !zmienne sterowania krokiem czasowym
      real(kind=8) :: eta, eps, eps2, xmin, xmax, ymin, ymax, zmin, zmax

      !zmienne numeryczne programu
      real(kind=8) :: RS, n_orbit, tend, dx_cell, dy_cell, dz_cell, dx, dy, dz

		!zmienne orbity poczatkowej
		real(kind=8) :: x0, y0, z0, vx0, vy0, vz0, e, mu, pol_os

      !zmienne
      real(kind=8) :: dth, a, ax, ay, az, axx, ayy, azz, t, dt, energy, energia_poczatkowa, denergy, L, dL, L_poczatkowe, zero


      !tablice
      real(kind=8),dimension(:,:,:),allocatable :: pot
      real(kind=8),dimension(1,3) :: polozenie, predkosc, predkosc_h

      !wektory
      integer,dimension(1,3) :: cell

      !wskazniki
      procedure(df_dx),pointer :: df_dx_p
      procedure(df_dy),pointer :: df_dy_p
      procedure(df_dz),pointer :: df_dz_p
      procedure(d2f_dx2),pointer :: d2f_dx2_p
      procedure(d2f_dy2),pointer :: d2f_dy2_p
      procedure(d2f_dz2),pointer :: d2f_dz2_p
      procedure(d2f_dxdy),pointer :: d2f_dxdy_p
      procedure(d2f_dxdz),pointer :: d2f_dxdz_p
      procedure(d2f_dydz),pointer :: d2f_dydz_p

      zero = 0.0



      order = 2                      !rzad pochodnych (2,4,6)

      eta	=	35.0				      !dokladnosc - wykorzystywane przy obliczaniu zmiennego kroku czasowego
      eps	=	1.0e-6
      eps2  =  0.00

      e	=	0.9				      !ekscentrycznosc
      mu	=	1.0				      !parametr grawitacyjny !G*(M+mp)



      !warunki poczatkowe
      x0	= 1.0				      !polozenie poczatkowe
      y0	= 0.0
      z0 = 0.0
      vx0 =	0.0				   !predkosc poczatkowa
      vy0 = predkosc_poczatkowa(x0,y0,vx0,e,mu) !sqrt(G*M/(x0))       !kolowa
      vz0 = 0.0

      nx_cell = 300
      ny_cell = 300
      nz_cell = 300

      xmin = -3.0
      xmax = 3.0
      ymin = -3.0
      ymax = 3.0
      zmin = -3.0
      zmax = 3.0


      !-------------------------------------------------------------------------------

      n_orbit = 20.0			      !ilosc orbit do pocalkowania

      pol_os = abs(x0/2.0)
      RS = 2.0*pi*pol_os		   !'czas' jednej orbity
      tend = n_orbit * RS 	      !czas koncowy



      polozenie(1,1) = x0					!polozenie poczatkowe
      polozenie(1,2) = y0
      polozenie(1,3) = z0
         write(*,'(A23,f12.8,A1,f12.8)') "#Polozenie poczatkowe: ",polozenie(1,1),",",polozenie(1,2),",",polozenie(1,3)

      predkosc(1,1) = vx0					!predkosc poczatkowa
      predkosc(1,2) = vy0
      predkosc(1,3) = vz0
         write(*,'(A23,f12.8,A1,f12.8)') "#Predkosc poczatkowa: ",predkosc(1,1),",",predkosc(1,2),",",predkosc(1,3)


      energia_poczatkowa = 0.5 * ( predkosc(1,1)**2 + predkosc(1,2)**2 +predkosc(1,3)**2) +&
                        phi_pm(polozenie(1,1),polozenie(1,2), polozenie(1,3), eps)
         write(*,'(A23,f12.8)') "#Energia poczatkowa: ",energia_poczatkowa

      L_poczatkowe = mom_pedu(polozenie,predkosc)
         write(*,'(A23,f12.8)') "#Moment pedu L0: ",L_poczatkowe



      allocate( pot(nx_cell, ny_cell, nz_cell) )



      call siatka(pot, xmin, xmax, ymin, ymax, zmin, zmax, nx_cell, ny_cell, &
            nz_cell, dx_cell, dy_cell, dz_cell, eps2)
         write(*,*) "#Obliczono potencjal na siatce."
         write(*,*) "#Oczko siatki wynosi: ", dx_cell, dy_cell, dz_cell

   !   open(unit=2, file='potencjal.dat')
   !      do i = 1, nx_cell, 1
   !         do j = 1, ny_cell, 1
   !            do k=1, nz_cell, 1
   !               write(2,*) i, j, k, pot(i,j,k)
   !            enddo
   !         enddo
   !      enddo
   !   close(2)



      call komorka(polozenie, cell, xmin, ymin, zmin, dx_cell, dy_cell, dz_cell)
         write(*,*) "#Obliczono numer komorki: ", cell

      call check_ord(order, df_dx_p, d2f_dx2_p, df_dy_p, d2f_dy2_p,& 
                  df_dz_p, d2f_dz2_p, d2f_dxdy_p, d2f_dxdz_p, d2f_dydz_p)

      call interpolacja(cell, polozenie, ax, ay, az, pot, nx_cell, ny_cell, &
            nz_cell, xmin, ymin, zmin, dx_cell, dy_cell, dz_cell, dx, dy, dz)


      a = sqrt(ax**2+ay**2+ az**2)
         write(*,*) "#ax=",ax," ay=", ay," az=", az, " a=", a

      ! Zmienny krok czasowy 
      dt=0.001
      t = 0.0
      !dt = sqrt(2.0*eta*eps/a)		
      dth = dt*0.5

      energy = energia_poczatkowa
      denergy = 0.0

      L = L_poczatkowe
      dL = 0.0


      orbita = 1

      !Glowna petla programu
      n = 1
      open(unit=1,file='dane.dat')
         write(1,*) "#n   t     dt      x   y  vx  vy energy denergy L  dL orbita ax ay ax0 ay0"

         do while (t<tend)
            !kick-drift-kick
            write(1,'(i7,12(1X,f16.8),1X,i6,6(1X,f16.12))') &
            n,t,dt,polozenie,predkosc,energy,denergy,L,dL,orbita,ax,ay,az,axx,ayy,azz

            !1.KICK
            !ax = axx
            !ay = ayy
            predkosc_h = kick(predkosc, ax, ay, az, dth)
            !predkosc_h = kick(predkosc, zero, zero, dth)
            !2.DRIFT
         	polozenie = drift(polozenie, predkosc_h, dt)

            !Przyspieszenie
            axx = -der_x(polozenie,eps)
            ayy = -der_y(polozenie,eps)
            azz = -der_z(polozenie,eps)

            !a	=	sqrt(ax**2+ay**2)

            call komorka(polozenie, cell, xmin, ymin, zmin, dx_cell, dy_cell, dz_cell)
            !write(*,*) n,"#Obliczono numer komorki: ", cell

            call interpolacja(cell, polozenie, ax, ay, az, pot, nx_cell, ny_cell, &
                  nz_cell, xmin, ymin, zmin, dx_cell, dy_cell, dz_cell, dx, dy, dz)
            !write(*,*) "             ax, ay", ax,ay
            a = sqrt(ax**2+ay**2+az**2) 
            !a = sqrt(axx**2 + ayy**2) 

            !3.KICK
            predkosc = kick(predkosc_h, ax, ay, az, dth)
            !predkosc = kick(predkosc_h,zero, zero,dth)
            !predkosc =	kick(predkosc_h,axx,ayy,dth)

            t = t + dt
           ! dt	= sqrt(2.0 * eta * eps /a)		!dt[n+1]
           ! dth = 0.5 * dt

            energy = 0.5*( predkosc(1,1)**2 + predkosc(1,2)**2 + predkosc(1,3)**2) + &
                     phi_pm(polozenie(1,1), polozenie(1,2), polozenie(1,3), eps)
            denergy = log(abs((energy - energia_poczatkowa) / energia_poczatkowa))
            L = (mom_pedu(polozenie,predkosc))
            dL	= log(abs((L-L_poczatkowe)/L_poczatkowe))

            n = n + 1
            orbita = int(t/RS) + 1
         end do

      close(1)

      deallocate(pot)


      write(*,*) "# ",(n-1)/n_orbit, " krokow na orbite"
      write(*,*) "#By zobaczyc rezultaty dzialania programu wykonaj polecenie 'gnuplot rysunki.plot'."
      !ewentualnie mozna skorzystac z ponizszej komendy
      !call system("gnuplot rysunki.plot")





      contains


      !potencjal masy punktowej
      function phi_pm(x, y, z, eps)
         implicit none
            real(kind=8) :: x, y, z, r, phi_pm, eps
               r = sqrt(x**2 + y**2 + z**2)
               phi_pm = -G*M / (sqrt(r**2 + eps**2))
      end function phi_pm

      !Pochodna wzgledem x
      function der_x(polozenie, d)
         implicit none
            real(kind=8) :: x, y, z, der_x, d
            real(kind=8),dimension(1,3) :: polozenie
            x = polozenie(1,1)
            y = polozenie(1,2)
            z = polozenie(1,3)
            der_x = ( phi_pm(x+d, y, z, eps) - phi_pm(x-d, y, z, eps) ) / (2.0*d)
      end function der_x

      !Pochodna wzgledem x
      function der_y(polozenie, d)
         implicit none
            real(kind=8) :: x, y, z, der_y, d
            real(kind=8),dimension(1,3) :: polozenie
            x = polozenie(1,1)
            y = polozenie(1,2)
            z = polozenie(1,3)
            der_y	= ( phi_pm(x, y+d, z,eps) - phi_pm(x, y-d, z, eps) ) / (2.0*d)
      end function der_y

      !Pochodna wzgledem z
      function der_z(polozenie, d)
         implicit none
            real(kind=8) :: x, y, z, der_z, d
            real(kind=8),dimension(1,3) :: polozenie
            x = polozenie(1,1)
            y = polozenie(1,2)
            z = polozenie(1,3)
            der_z	= ( phi_pm(x, y, z+d, eps) - phi_pm(x, y, z-d, eps) ) / (2.0*d)
      end function der_z

      !Moment pedu
      function mom_pedu(polozenie, predkosc)
         implicit none
            real(kind=8) :: mom_pedu, r_dl, p_dl, x, y, z, vx, vy, vz
            real(kind=8),dimension(1,3) :: polozenie, predkosc
            x = polozenie(1,1)
            y = polozenie(1,2)
            z = polozenie(1,3)
            vx = predkosc(1,1)
            vy = predkosc(1,2)
            vz = predkosc(1,3)	
            r_dl = sqrt(x**2 + y**2 + z**2)
            p_dl = sqrt(vx**2 + vy**2 + vz**2)
            mom_pedu = r_dl * p_dl * sqrt( 1.0 - ( (x*vx + y*vy + z*vz) / (r_dl*p_dl))**2)
      end function mom_pedu

      !Drift
      function drift(polozenie, predkosc, t)
         implicit none
            real(kind=8) :: x, y, z, vx, vy, vz, t
            real(kind=8),dimension(1,3) :: drift, polozenie, predkosc
               x = polozenie(1,1)
               y = polozenie(1,2)
               z = polozenie(1,3)
               vx = predkosc(1,1)
               vy = predkosc(1,2)
               vz = predkosc(1,3)
               drift(1,1) = x + vx*t
               drift(1,2) = y + vy*t
               drift(1,3) = z + vz*t
      end function drift

      !Kick
      function kick(predkosc, ax, ay, az, t)
         implicit none
            real(kind=8) :: vx, vy, vz, ax, ay, az,t
            real(kind=8),dimension(1,3) :: kick, predkosc
               vx = predkosc(1,1)
               vy = predkosc(1,2)
               vz = predkosc(1,3)
               kick(1,1) = vx + ax*t
               kick(1,2) = vy + ay*t
               kick(1,3) = vz + az*t
      end function kick


      !predkosc poczatkowa dla zadanego a (polos wielka) i e (mimosrod)
      !w punkcie startowym znajdujacym sie w apocentrum i przy zalozeniu
      !predkosci poczatkowej Vx0=0.0
      function predkosc_poczatkowa(x, y, vx, e, mu)
         implicit none
            real(kind=8) :: x, y, vx, e, mu, r, v2, predkosc_poczatkowa
            r = sqrt(x**2+y**2)
            v2 = (2.0 * mu * (1 - e**2) ) / (r * (r**2 + (1 - e**2) ) )
            predkosc_poczatkowa = sqrt(v2)
      end function predkosc_poczatkowa


      !------------------------------------------------------------------
      !funkcje uzywane do interpolacji

      subroutine siatka(pot, xmin, xmax, ymin, ymax, zmin, zmax, nx_cell, ny_cell, nz_cell, dx_cell, dy_cell, dz_cell, eps2)
         implicit none
            integer :: i, j, k
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            real(kind=8),intent(in) :: xmin, xmax, ymin, ymax, zmin, zmax, eps2
            real(kind=8),intent(out) :: dx_cell, dy_cell, dz_cell
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(out) :: pot
               dx_cell = (xmax - xmin) / (float(nx_cell))
               dy_cell = (ymax - ymin) / (float(ny_cell))
               dz_cell = (zmax - zmin) / (float(nz_cell))

               do i = 1, nx_cell, 1
                  do j = 1, ny_cell, 1
                     do k = 1, nz_cell, 1
                        pot(i, j, k) = phi_pm(xmin + i*dx_cell, ymin + j*dy_cell, zmin + k*dz_cell, eps2)
                     enddo
                  enddo
               enddo
               
      end subroutine siatka


      subroutine check_ord(order, df_dx_p, d2f_dx2_p, df_dy_p, d2f_dy2_p,& 
                  df_dz_p, d2f_dz2_p, d2f_dxdy_p, d2f_dxdz_p, d2f_dydz_p)
         implicit none
            integer,intent(in) :: order
            procedure(df_dx),pointer :: df_dx_p
            procedure(df_dy),pointer :: df_dy_p
            procedure(df_dz),pointer :: df_dz_p
            procedure(d2f_dx2),pointer :: d2f_dx2_p
            procedure(d2f_dy2),pointer :: d2f_dy2_p
            procedure(d2f_dz2),pointer :: d2f_dz2_p
            procedure(d2f_dxdy),pointer :: d2f_dxdy_p
            procedure(d2f_dxdz),pointer :: d2f_dxdz_p
            procedure(d2f_dydz),pointer :: d2f_dydz_p
               if(.not.((order==2) .or. (order==4) .or. (order==6) ) ) then
                  write(*,*) "#Wybrano zla dokladnosc pochodnych: ", order
                  write(*,*) "#Dostepna dokladnosc: 2,4 lub 6."
                  write(*,*) "#Koniec dzialania programu. Nic nie zrobiono."
                  stop
               else
                  select case (order)
                     case(2)
                        df_dx_p => df_dx_o2
                        df_dy_p => df_dy_o2
                        df_dz_p => df_dz_o2
                        d2f_dx2_p => d2f_dx2_o2
                        d2f_dy2_p => d2f_dy2_o2
                        d2f_dz2_p => d2f_dz2_o2
                        d2f_dxdy_p => d2f_dxdy_o2
                        d2f_dxdz_p => d2f_dxdz_o2
                        d2f_dydz_p => d2f_dydz_o2
                     case(4)
                        df_dx_p => df_dx_o4
                        df_dy_p => df_dy_o4
                        df_dz_p => df_dz_o4
                        d2f_dx2_p => d2f_dx2_o4
                        d2f_dy2_p => d2f_dy2_o4
                        d2f_dz2_p => d2f_dz2_o4
                        d2f_dxdy_p => d2f_dxdy_o4
                        d2f_dxdz_p => d2f_dxdz_o4
                        d2f_dydz_p => d2f_dydz_o4
                     case(6)
                        df_dx_p => df_dx_o6
                        df_dy_p => df_dy_o6
                        df_dz_p => df_dz_o6
                        d2f_dx2_p => d2f_dx2_o6
                        d2f_dy2_p => d2f_dy2_o6
                        d2f_dz2_p => d2f_dz2_o6
                        d2f_dxdy_p => d2f_dxdy_o6
                        d2f_dxdz_p => d2f_dxdz_o6
                        d2f_dydz_p => d2f_dydz_o6
                  end select
               endif
      end subroutine check_ord


      subroutine komorka(polozenie,cell,xmin,ymin,zmin,dx_cell,dy_cell,dz_cell)
         implicit none
            integer,dimension(1,3),intent(out) :: cell
            real(kind=8),dimension(1,3),intent(in) :: polozenie
            real(kind=8),intent(in) :: xmin,ymin,zmin,dx_cell,dy_cell, dz_cell
               if ((polozenie(1,1)<xmin) .or. (polozenie(1,1)>xmax) .or. &
               (polozenie(1,2)<ymin) .or. (polozenie(1,2) >ymax) .or.&
               (polozenie(1,3)<zmin) .or. (polozenie(1,3) >zmax)) then
                  write(*,*) "#Przekroczono granice!. Zatrzymano!!!"
                  stop
               endif
               cell(1,1) = floor( (polozenie(1,1) - xmin - 0.5*dx_cell) / dx_cell) + 1
               cell(1,2) = floor( (polozenie(1,2) - ymin - 0.5*dy_cell) / dy_cell) + 1
               cell(1,3) = floor( (polozenie(1,3) - zmin - 0.5*dz_cell) / dz_cell) + 1

      end subroutine komorka


      subroutine interpolacja(cell, polozenie, ax, ay, az, pot, nx_cell, ny_cell, &
                     nz_cell, xmin, ymin, zmin, dx_cell, dy_cell, dz_cell, dx, dy, dz)
         implicit none
            real(kind=8),dimension(1,3),intent(in) :: polozenie
            integer,dimension(1,3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            real(kind=8),dimension(nx_cell,ny_cell,nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: xmin,ymin,zmin,dx_cell, dy_cell, dz_cell
            real(kind=8),intent(out) :: ax,ay,az
            real(kind=8),intent(out) :: dx,dy, dz

               dx = -(cell(1,1)*dx_cell + xmin - polozenie(1,1))
               dy = -(cell(1,2)*dy_cell + ymin - polozenie(1,2))
               dz = -(cell(1,3)*dz_cell + zmin - polozenie(1,3))

               ax = -( df_dx_p(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell) + &
                  d2f_dx2_p(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell) * dx + &
                  d2f_dxdy_p(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dy_cell) * dy + &
                  d2f_dxdz_p(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dz_cell) * dz)
               ay = -( df_dy_p(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell) + &
                  d2f_dy2_p(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell) * dy + &
                  d2f_dxdy_p(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dy_cell) * dx + &
                  d2f_dydz_p(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell, dz_cell) * dz)
               az = -( df_dz_p(cell, pot, nx_cell, ny_cell, nz_cell, dz_cell) + &
                  d2f_dz2_p(cell, pot, nx_cell, ny_cell, nz_cell, dz_cell) * dz + &
                  d2f_dxdz_p(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dz_cell) * dx +&
                  d2f_dydz_p(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell, dz_cell) * dy)

      end subroutine interpolacja


      function df_dx_o2(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell
            real(kind=8) :: df_dx_o2

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^2)
            df_dx_o2 = ( pot(i+1, j, k) - pot(i-1, j, k) ) / (2.0*dx_cell)

      end function df_dx_o2


      function df_dx_o4(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell
            real(kind=8),target :: df_dx_o4

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^4)
            df_dx_o4 = ( 2.0* (pot(i+1, j, k) - pot(i-1, j, k) ) ) / (3.0*dx_cell) - &
                     ( pot(i+2, j, k) - pot(i-2, j, k) ) / (12.0*dx_cell)

      end function df_dx_o4


      function df_dx_o6(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell
            real(kind=8),target :: df_dx_o6

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^6)
            df_dx_o6 = (3.0*(pot(i+1, j, k)-pot(i-1, j, k)))/(4.0*dx_cell) - &
                     (3.0*(pot(i+2, j, k)-pot(i-2, j, k)))/(20.0*dx_cell) +&
                     (pot(i+3, j, k)-pot(i-3, j, k))/(60.0*dx_cell)

      end function df_dx_o6


      function df_dy_o2(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dy_cell
            real(kind=8),target :: df_dy_o2

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^2)
            df_dy_o2 = (pot(i, j+1, k) - pot(i, j-1, k) ) / (2.0*dy_cell)

      end function df_dy_o2


      function df_dy_o4(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dy_cell
            real(kind=8),target :: df_dy_o4

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^4)
            df_dy_o4 = ( 2.0 * ( pot(i, j+1, k) - pot(i, j-1, k) ) ) / (3.0*dy_cell) - &
                     ( pot(i, j+2, k) - pot(i, j-2, k) ) / (12.0*dy_cell)

      end function df_dy_o4


      function df_dy_o6(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dy_cell
            real(kind=8),target :: df_dy_o6

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^6)
            df_dy_o6 = (3.0*(pot(i, j+1, k) - pot(i, j-1, k) ) ) / (4.0*dy_cell) - &
                     (3.0*(pot(i, j+2, k) - pot(i, j-2, k) ) ) / (20.0*dy_cell) +&
                     (pot(i, j+3, k) - pot(i, j-3, k) ) / (60*dy_cell)

      end function df_dy_o6


      function df_dz_o2(cell, pot, nx_cell, ny_cell, nz_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dz_cell
            real(kind=8) :: df_dz_o2

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^2)
            df_dz_o2 = ( pot(i, j, k+1) - pot(i, j, k-1) ) / (2.0*dz_cell)

      end function df_dz_o2


      function df_dz_o4(cell, pot, nx_cell, ny_cell, nz_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dz_cell
            real(kind=8),target :: df_dz_o4

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^4)
            df_dz_o4 = ( 2.0* (pot(i, j, k+1) - pot(i, j, k-1) ) ) / (3.0*dz_cell) - &
                     ( pot(i, j, k+2) - pot(i, j, k-2) ) / (12.0*dz_cell)

      end function df_dz_o4 


      function df_dz_o6(cell, pot, nx_cell, ny_cell, nz_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dz_cell
            real(kind=8),target :: df_dz_o6

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^6)
            df_dz_o6 = (3.0*( pot(i, j, k+1) - pot(i, j, k-1) ) ) / (4.0*dz_cell) - &
                     (3.0 * ( pot(i, j, k+2) - pot(i, j, k-2) ) ) / (20.0*dz_cell) +&
                     ( pot(i, j, k+3) - pot(i, j, k-3) ) / (60.0*dz_cell)

      end function df_dz_o6
         
      function d2f_dx2_o2(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer :: i, j, k
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell
            real(kind=8),target :: d2f_dx2_o2

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^2)
            d2f_dx2_o2 = (pot(i+1, j, k) - 2.0*pot(i, j, k) + pot(i-1, j, k) ) / (dx_cell**2)

      end function d2f_dx2_o2


      function d2f_dx2_o4(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer :: i, j, k
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell
            real(kind=8),target :: d2f_dx2_o4

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^4)
            d2f_dx2_o4 = 4.0 * ( pot(i+1, j, k) + pot(i-1, j, k) - &
                     2.0 * pot(i, j, k) ) / (3.0*dx_cell**2) - &
                     ( pot(i+2, j, k) + pot(i-2, j, k) - 2.0 * pot(i, j, k) ) / (12.0*dx_cell**2)

      end function d2f_dx2_o4


      function d2f_dx2_o6(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer :: i, j, k
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell
            real(kind=8),target :: d2f_dx2_o6

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^6)
            d2f_dx2_o6 = 3.0*( pot(i+1, j, k) + pot(i-1, j, k) - &
                        2.0*pot(i, j, k) ) / (2.0*dx_cell**2) - &
                        3.0*( pot(i+2, j, k) + pot(i-2, j, k) - &
                        2.0*pot(i,j, k) ) / (20.0*dx_cell**2) + &
                        ( pot(i+3, j, k) + pot(i-3,j, k) - 2.0*pot(i,j, k) ) / (90.0*dx_cell**2)

      end function d2f_dx2_o6


      function d2f_dy2_o2(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dy_cell
            real(kind=8),target :: d2f_dy2_o2

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^2)
            d2f_dy2_o2 = ( pot(i, j+1, k) - 2.0*pot(i, j, k) + pot(i, j-1, k) ) / (dy_cell**2)

      end function d2f_dy2_o2


      function d2f_dy2_o4(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dy_cell
            real(kind=8),target :: d2f_dy2_o4

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^4)
            d2f_dy2_o4 = 4.0*( pot(i, j+1, k) + pot(i, j-1, k) - &
                     2.0*pot(i, j, k) ) / (3.0*dy_cell**2) - &
                     ( pot(i, j+2, k) + pot(i, j-2, k) - 2.0*pot(i, j, k) ) / (12.0*dy_cell**2)

      end function d2f_dy2_o4


      function d2f_dy2_o6(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dy_cell
            real(kind=8),target :: d2f_dy2_o6

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^6)
            d2f_dy2_o6 = 3.0*( pot(i, j+1, k) + pot(i, j-1, k) - &
                        2.0*pot(i, j, k) ) / (2.0*dy_cell**2) - &
                        3.0* (pot(i, j+2, k) + pot(i, j-2, k) - &
                        2.0*pot(i, j, k) ) / (20.0*dy_cell**2) + &
                        ( pot(i, j+3, k) + pot(i, j-3, k) - 2.0*pot(i, j, k) ) / (90.0*dy_cell**2)

      end function d2f_dy2_o6


      function d2f_dz2_o2(cell, pot, nx_cell, ny_cell, nz_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dz_cell
            real(kind=8),target :: d2f_dz2_o2

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^2)
            d2f_dz2_o2 = ( pot(i, j, k+1) - 2.0*pot(i, j, k) + pot(i, j, k-1) ) / (dz_cell**2)

      end function d2f_dz2_o2


      function d2f_dz2_o4(cell, pot, nx_cell, ny_cell, nz_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dz_cell
            real(kind=8),target :: d2f_dz2_o4

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^4)
            d2f_dz2_o4 = 4.0*( pot(i, j, k+1) + pot(i, j, k-1) - &
                     2.0*pot(i, j, k) ) / (3.0*dz_cell**2) - &
                     ( pot(i, j, k+2) + pot(i, j, k-2) - 2.0*pot(i, j, k) ) / (12.0*dz_cell**2)

      end function d2f_dz2_o4


      function d2f_dz2_o6(cell, pot, nx_cell, ny_cell, nz_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dz_cell
            real(kind=8),target :: d2f_dz2_o6

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^6)
            d2f_dz2_o6 = 3.0*( pot(i, j, k+1) + pot(i, j, k-1) - &
                        2.0*pot(i, j, k) ) / (2.0*dz_cell**2) - &
                        3.0* (pot(i, j, k+2) + pot(i, j, k-2) - &
                        2.0*pot(i, j, k) ) / (20.0*dz_cell**2) + &
                        ( pot(i, j, k+3) + pot(i, j, k-3) - 2.0*pot(i, j, k) ) / (90.0*dz_cell**2)

      end function d2f_dz2_o6


      function d2f_dxdy_o2(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dy_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell, dy_cell
            real(kind=8),target :: d2f_dxdy_o2
           
            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)
            
            !o(R^2)
            d2f_dxdy_o2 = ( pot(i+1, j+1, k) - pot(i+1, j-1, k) - &
                           pot(i-1, j+1, k) + pot(i-1, j-1, k) ) / (4.0*dx_cell*dy_cell)

      end function d2f_dxdy_o2


      function d2f_dxdy_o4(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dy_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell, dy_cell
            real(kind=8),target :: d2f_dxdy_o4

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^4)
            d2f_dxdy_o4 = ( pot(i+1, j+1, k) + pot(i-1, j-1, k) - pot(i+1, j-1, k) - &
                        pot(i-1, j+1, k) ) / (3.0*dx_cell*dy_cell) - &
                        ( pot(i+2, j+2, k) + pot(i-2, j-2, k) - pot(i+2, j-2, k) - &
                        pot(i-2, j+2, k) ) / (48.0*dx_cell*dy_cell)

      end function d2f_dxdy_o4


      function d2f_dxdy_o6(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dy_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell, dy_cell
            real(kind=8),target :: d2f_dxdy_o6

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^6)
            d2f_dxdy_o6 = 3.0*( pot(i+1, j+1, k) + pot(i-1,j-1, k) - &
                        pot(i+1,j-1, k) - pot(i-1, j+1, k) ) / (8.0*dx_cell*dy_cell) - &
                        3.0*( pot(i+2, j+2, k) + pot(i-2, j-2, k) - &
                        pot(i+2, j-2, k) - pot(i-2, j+2, k) ) / (80.0*dx_cell*dy_cell) + &
                        ( pot(i+3,j+3, k) + pot(i-3, j-3, k) - pot(i+3, j-3, k) - &
                        pot(i-3, j+3, k) ) / (360.0*dx_cell*dy_cell)

      end function d2f_dxdy_o6


      function d2f_dxdz_o2(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell, dz_cell
            real(kind=8),target :: d2f_dxdz_o2
           
            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)
            
            !o(R^2)
            d2f_dxdz_o2 = ( pot(i+1, j, k+1) - pot(i+1, j, k-1) - &
                           pot(i-1, j, k+1) + pot(i-1, j, k-1) ) / (4.0*dx_cell*dz_cell)

      end function d2f_dxdz_o2


      function d2f_dxdz_o4(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell, dz_cell
            real(kind=8),target :: d2f_dxdz_o4

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^4)
            d2f_dxdz_o4 = ( pot(i+1, j, k+1) + pot(i-1, j, k-1) - pot(i+1, j, k-1) - &
                        pot(i-1, j, k+1) ) / (3.0*dx_cell*dz_cell) - &
                        ( pot(i+2, j, k+2) + pot(i-2, j, k-2) - pot(i+2, j, k-2) - &
                        pot(i-2, j, k+2) ) / (48.0*dx_cell*dz_cell)

      end function d2f_dxdz_o4


      function d2f_dxdz_o6(cell, pot, nx_cell, ny_cell, nz_cell, dx_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dx_cell, dz_cell
            real(kind=8),target :: d2f_dxdz_o6

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^6)
            d2f_dxdz_o6 = 3.0*( pot(i+1, j, k+1) + pot(i-1, j, k-1) - &
                        pot(i+1,j, k-1) - pot(i-1, j, k+1) ) / (8.0*dx_cell*dz_cell) - &
                        3.0*( pot(i+2, j, k+2) + pot(i-2, j, k-2) - &
                        pot(i+2, j, k-2) - pot(i-2, j, k+2) ) / (80.0*dx_cell*dz_cell) + &
                        ( pot(i+3,j, k+3) + pot(i-3, j, k-3) - pot(i+3, j, k-3) - &
                        pot(i-3, j, k+3) ) / (360.0*dx_cell*dz_cell)

      end function d2f_dxdz_o6


      function d2f_dydz_o2(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dy_cell, dz_cell
            real(kind=8),target :: d2f_dydz_o2
           
            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)
            
            !o(R^2)
            d2f_dydz_o2 = ( pot(i, j+1, k+1) - pot(i, j+1, k-1) - &
                           pot(i, j-1, k+1) + pot(i, j-1, k-1) ) / (4.0*dy_cell*dz_cell)

      end function d2f_dydz_o2


      function d2f_dydz_o4(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dy_cell, dz_cell
            real(kind=8),target :: d2f_dydz_o4

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^4)
            d2f_dydz_o4 = ( pot(i, j+1, k+1) + pot(i, j-1, k-1) - pot(i, j+1, k-1) - &
                        pot(i, j-1, k+1) ) / (3.0*dy_cell*dz_cell) - &
                        ( pot(i, j+2, k+2) + pot(i, j-2, k-2) - pot(i, j+2, k-2) - &
                        pot(i, j-2, k+2) ) / (48.0*dy_cell*dz_cell)

      end function d2f_dydz_o4


      function d2f_dydz_o6(cell, pot, nx_cell, ny_cell, nz_cell, dy_cell, dz_cell)
         implicit none
            integer,dimension(1, 3),intent(in) :: cell
            integer,intent(in) :: nx_cell, ny_cell, nz_cell
            integer :: i, j, k
            real(kind=8),dimension(nx_cell, ny_cell, nz_cell),intent(in) :: pot
            real(kind=8),intent(in) :: dy_cell, dz_cell
            real(kind=8),target :: d2f_dydz_o6

            i = cell(1, 1)
            j = cell(1, 2)
            k = cell(1, 3)

            !o(R^6)
            d2f_dydz_o6 = 3.0*( pot(i, j+1, k+1) + pot(i, j-1,k-1) - &
                        pot(i, j+1, k-1) - pot(i, j-1, k+1) ) / (8.0*dy_cell*dz_cell) - &
                        3.0*( pot(i, j+2, k+2) + pot(i, j-2, k-2) - &
                        pot(i, j+2, k-2) - pot(i, j-2, k+2) ) / (80.0*dy_cell*dz_cell) + &
                        ( pot(i, j+3,k+3) + pot(i, j-3, k-3) - pot(i, j+3, k-3) - &
                        pot(i, j-3, k+3) ) / (360.0*dy_cell*dz_cell)

      end function d2f_dydz_o6
!koniec programu
end program pm_KDK
