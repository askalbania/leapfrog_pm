set terminal postscript color enhanced
set encoding iso_8859_2
set output '| ps2pdf - rysunki.pdf'
set size square
set lmargin 0.10
set rmargin 0.1
set bmargin 1.1
set tmargin 2.1
set multiplot
#
#dE------------------------------------
set size 0.5,0.5
set origin 0.0,0.0
unset xrange
unset yrange
set grid
set title "Zmiana energii"
set xlabel "t"
set ylabel "{/Symbol D}E"
plot 'dane.dat' u 2:9 w l notitle
#dL------------------------------------
set size 0.5,0.5
set origin 0.6,0.0
set grid
set title 'Zmiana momentu p� du'
set xlabel "t"
set ylabel "{/Symbol D}L"
#set output 'dL.png'
plot 'dane.dat' u 2:11 w l notitle
#
#tor ruchu - wszystkie orbity----------
set size 0.5,0.5
set origin 0.0,0.6
set xrange [-0.5:2.0]
set yrange [-1.25:1.25]
set grid
set title "Orbity"
set xlabel "X"
set ylabel "Y"
plot 'dane.dat' u 4:5 w l notitle
#tor ruchu - co 20 orbita----------
#
set size 0.5,0.5
set origin 0.6,0.6
set xrange [-0.5:2.0]
set yrange [-1.25:1.25]
set grid
set title "Orbity-co 20."
set xlabel "X"
set ylabel "Y"
plot 'dane.dat' u 4:(int($12) % 20 == 0 ? $5 : 1/0) w l notitle
unset multiplot
