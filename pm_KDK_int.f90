program pm_KDK
	!Program calkujacy potencjal masy punktowej metoda leapfrog KDK przy zmiennym kroku calkowania
   !uzywajac interpolacji potencjalu na siatce
	
	!Autor: A. Skalbania, luty 2014
   
   

	implicit none
		integer					:: n, i, j, orbita,n_cell
		!zmienne 'dynamiczne'
		real, parameter			:: d=1.0e-6, G=1.0, M=1.0, mp=1.0, pi=3.14159265358979

		!zmienne sterowania krokiem czasowym
		real			:: eta, eps,xmin,xmax,ymin,ymax,d_cell

		!zmienne numeryczne programu
		real			:: RS, n_orbit, tend

		!zmienne orbity poczatkowej
		real			:: x0, y0, vx0, vy0, e, mu, pol_os

		!zmienne
		real			::dth, a, ax, ay
		

		!tablice
		real,dimension(1,2)  :: polozenie, predkosc
      integer,dimension(1,2)  :: nr_kom
		real  :: t, dt, energy, energia_poczatkowa, denergy, L, dL, L_poczatkowe
      real,dimension(:,:),allocatable   :: potencjal
		!wektory
		real,dimension(1,2)  :: predkosc_h, tmp


		

		eta	=	35.0				      !dokladnosc - wykorzystywane przy obliczaniu zmiennego kroku czasowego
		eps	=	1.0e-5

		e	=	0.9				      !ekscentrycznosc
		mu	=	1.0				      !parametr grawitacyjny !G*(M+mp)



		!warunki poczatkowe
		x0	=	2.0				      !polozenie poczatkowe
		y0	=	0.0
		vx0	=	0.0				      !predkosc poczatkowa
		vy0	=	predkosc_poczatkowa(x0,y0,vx0,e,mu)
!-------------------------------------------------------------------------------

		pol_os	=	x0/2.0
		
		RS	=	2.0*pi*pol_os		!'czas' jednej orbity
		n_orbit	=	200.0			      !ilosc orbit do pocalkowania

		tend	=	n_orbit * RS 	   !czas koncowy

     
		write(*,'(A23,f12.1)') "Liczba orbit: ", n_orbit



	polozenie(1,1)	=	x0					!polozenie poczatkowe
	polozenie(1,2)	=	y0
		write(*,'(A23,f12.8,A1,f12.8)') "Polozenie poczatkowe: ",polozenie(1,1),",",polozenie(1,2)

	predkosc(1,1)	=	vx0					!predkosc poczatkowa
	predkosc(1,2)	=	vy0
		write(*,'(A23,f12.8,A1,f12.8)') "Predkosc poczatkowa: ",predkosc(1,1),",",predkosc(1,2)


	energia_poczatkowa	=	0.5*(predkosc(1,1)**2 + predkosc(1,2)**2) + phi_pm(polozenie(1,1),polozenie(1,2),eps)
		write(*,'(A23,f12.8)') "Energia poczatkowa: ",energia_poczatkowa

	L_poczatkowe=mom_pedu(polozenie,predkosc)
		write(*,'(A23,f12.8)') "Moment pedu L0: ",L_poczatkowe
      
      
      
   xmin=-3.0
   xmax=3.0
   ymin=xmin
   ymax=xmax
   n_cell=40
   
   allocate(potencjal(n_cell,n_cell))
   
	!przyspieszenie poczatkowe
	!ax       =  -der_x(polozenie,eps)
	!ay       =  -der_y(polozenie,eps)
	!a        =  sqrt(ax**2+ay**2)
   
   
   call siatka(potencjal,xmin,xmax,n_cell,d_cell)
   write(*,*) "Obliczono potencjal na siatce."
   
   
   call komorka(polozenie,nr_kom,xmin)
   write(*,*) "Obliczono numer komorki: ", nr_kom(1,1), nr_kom(1,2)
   call interpolacja(nr_kom,polozenie,ax,ay,potencjal,n_cell,xmin,d_cell)
   
   
   a        =  sqrt(ax**2+ay**2)
   write(*,*) "ax=",ax," ay=", ay, " a=", a
   
	! Zmienny krok czasowy 
	dt	=	sqrt(2.0*eta*eps/a)		
	dth	=	dt*0.5

	t	=	0.0
	energy	=	energia_poczatkowa
	denergy	=	0.0
	L	=	L_poczatkowe
   dL	=	0.0

	orbita	=	1


	!Glowna petla programu
	n	=	1
	open(unit=1,file='dane.dat')
	write(1,*) "#      n      t      dt      x     y      vx     vy     energy      denergy     L     dL     orbita"
		do while (t<tend)
			!kick-drift-kick
			write(1,'(i7,10(5X,f16.8),5X,i6)') n,t,dt,polozenie,predkosc,energy,denergy,L,dL,orbita
			!
			!1.KICK
			predkosc_h=	kick(predkosc,ax,ay,dth)
			!
			!2.DRIFT
			polozenie=	drift(polozenie,predkosc_h,dt)
			!
			!Przyspieszenie
			!ax	=	-der_x(polozenie,eps)
			!ay	=	-der_y(polozenie,eps)
			!
			!a	=	sqrt(ax**2+ay**2)
			!
         write(*,*) n, " Polozenie: ", polozenie(1,1), polozenie(1,2)
         !
         call komorka(polozenie,nr_kom,xmin)
         write(*,*) n," Obliczono numer komorki: ", nr_kom(1,1), nr_kom(1,2)
         !
         call interpolacja(nr_kom,polozenie,ax,ay,potencjal,n_cell,xmin,d_cell)
         !write(*,*) n," ax,ay: ", ax,ay 
         a	=	sqrt(ax**2+ay**2) 
			!
			!3.KICK
			predkosc=	kick(predkosc_h,ax,ay,dth)
			!
			!
			!
			t	=	t+dt
			dt	=	sqrt(2.0*eta*eps/a)		!dt[n+1]
			dth	=	0.5*dt
			!
			!
			!
			energy	=	0.5*(predkosc(1,1)**2+predkosc(1,2)**2) + phi_pm(polozenie(1,1),polozenie(1,2),eps)
         denergy	=	log(abs((energy	-	energia_poczatkowa)	/	energia_poczatkowa))
    		L	=	(mom_pedu(polozenie,predkosc))
    		dL	=	log(abs((L-L_poczatkowe)/L_poczatkowe))
    		n	=	n+1
    		orbita	=	int(t/RS)+1
		end do
	close(1)

   write(*,*) "By zobaczyc rezultaty dzialania programu wykonaj polecenie 'gnuplot rysunki.plot'."
   !ewentualnie mozna skorzystac z ponizszej komendy
   !call system("gnuplot rysunki.plot")
	
   !dalsza czesc programu-interpolacja numeryczna
   
   
   open(unit=2, file='potencjal.dat')
      do i=1,n_cell,1
         do j=1,n_cell,1
            write(2,*) i,j,potencjal(i,j)
         enddo
      enddo
   close(2)




contains


	!Potencjal masy punktowej
	function phi_pm(x, y, eps)
		real   :: x, y, r, phi_pm, eps
			r		=	sqrt(x**2+y**2)
			phi_pm	=	-G*M/(sqrt(r**2 + eps**2))
	end function phi_pm



	!Pochodna wzgledem x
	function der_x(polozenie,d)
		real   :: f, x, y, der_x, d
		real, dimension(1,2)   :: polozenie
			x	=	polozenie(1,1)
			y	=	polozenie(1,2)
			der_x	= 	(phi_pm(x+d,y,eps) - phi_pm(x-d,y,eps)) / (2.0*d)
	end function der_x



	!Pochodna wzgledem x
	function der_y(polozenie,d)
		real   :: f, x, y, der_y, d
		real, dimension(1,2)   :: polozenie
			x	=	polozenie(1,1)
			y	=	polozenie(1,2)
			der_y	=	(phi_pm(x,y+d,eps) - phi_pm(x,y-d,eps)) / (2.0*d)
	end function der_y



	!Moment pedu
	function mom_pedu(polozenie, predkosc)
		real   :: mom_pedu, r_dl, p_dl, x, y, vx, vy
		real, dimension(1,2)  :: polozenie, predkosc
         		x	=	polozenie(1,1)
         		y	=	polozenie(1,2)
         		vx	=	predkosc(1,1)
         		vy	=	predkosc(1,2)	
         		r_dl	=	sqrt(x**2+y**2)
         		p_dl	=	sqrt(vx**2+vy**2)
         		mom_pedu=	r_dl*p_dl*sqrt(1.0-((x*vx+y*vy) / (r_dl*p_dl))**2)
	end function mom_pedu



	!Drift
	function drift(polozenie,predkosc,t)
		real   :: x, y, vx, vy, t
		real, dimension(1,2)  :: drift, polozenie, predkosc
			x		=	polozenie(1,1)
			y		=	polozenie(1,2)
			vx		=	predkosc(1,1)
			vy		=	predkosc(1,2)
			drift(1,1)	=	x+vx*t
			drift(1,2)	=	y+vy*t
	end function drift



	!Kick
	function kick(predkosc, ax, ay, t)
		real   :: vx, vy, ax, ay, t
		real, dimension(1,2)  :: kick, predkosc
			vx		=	predkosc(1,1)
			vy		=	predkosc(1,2)
			kick(1,1)	=	vx+ax*t
			kick(1,2)	=	vy+ay*t
	end function kick



	!predkosc poczatkowa dla zadanego a (polos wielka) i e (mimosrod)
	!w punkcie startowym znajdujacym sie w apocentrum i przy zalozeniu
	!predkosci poczatkowej Vx0=0.0
	function predkosc_poczatkowa(x, y, vx, e, mu)
		real   :: x, y, vx, e, mu, r, v2, predkosc_poczatkowa
			r	=	sqrt(x**2+y**2)
			v2	=	((2.0*mu*(1-e**2))/(r*(r**2+(1-e**2))))
			predkosc_poczatkowa	=	sqrt(v2)
	end function predkosc_poczatkowa
   
   !------------------------------------------------------------------
   !funkcje uzywane do interpolacji
   
   subroutine siatka(potencjal,xmin,xmax,n_cell,d_cell)
   implicit none
      integer:: i,j
      integer, intent(in):: n_cell
      real, intent(in):: xmin,xmax
      real,intent(out)  :: d_cell
      real, dimension(n_cell,n_cell), intent(out):: potencjal
      
      d_cell=(xmax-xmin)/(float(n_cell))
      
      do i=1,n_cell,1
         do j=1,n_cell,1
            potencjal(i,j)=phi_pm(xmin+i*d_cell,xmin+j*d_cell,1.0e-4)
         enddo
      enddo
   end subroutine siatka
   
   subroutine komorka(polozenie,nr_kom,xmin)
      implicit none
         integer,dimension(1,2), intent(out)::nr_kom
         real, dimension(1,2), intent(in):: polozenie
         real, intent(in)::xmin
         nr_kom(1,1)=floor((polozenie(1,1)-xmin)/0.5)
         nr_kom(1,2)=floor((polozenie(1,2)-xmin)/0.5)
   end subroutine komorka
   
   
   subroutine interpolacja(nr_kom,polozenie,ax,ay,potencjal,n_cell,xmin,d_cell)
      implicit none
         real, dimension(1,2), intent(in) :: polozenie
         integer, dimension(1,2), intent(in):: nr_kom
         integer, intent(in):: n_cell
         real, dimension(n_cell,n_cell), intent(in)  :: potencjal
         real, intent(in):: xmin,d_cell
         real, intent(out) :: ax,ay
         real  :: dx,dy
         dx =  (xmin +  nr_kom(1,1) *  d_cell)  - polozenie(1,1)
         dy =  (xmin +  nr_kom(1,2) *  d_cell)  - polozenie(1,2)
         !write(*,*) "dx,dy: ", dx, dy
         ax =  df_dx(nr_kom,potencjal,n_cell,d_cell) + &
               d2f_dx2(nr_kom,potencjal,n_cell,d_cell)*dx + &
               d2f_dxdy(nr_kom,potencjal,n_cell,d_cell)*dx*dy
         ay =  df_dy(nr_kom,potencjal,n_cell,d_cell) + &
               d2f_dy2(nr_kom,potencjal,n_cell,d_cell)*dy + &
               d2f_dxdy(nr_kom,potencjal,n_cell,d_cell)*dy*dx
         !ax=(potencjal(nr_kom(1,1),nr_kom(1,2))+df_dx(nr_kom,potencjal)*dx+0.5*d2f_dx2(nr_kom,potencjal)*dx**2)
         !ay=(potencjal(nr_kom(1,1),nr_kom(1,2))+df_dy(nr_kom,potencjal)*dy+0.5*d2f_dx2(nr_kom,potencjal)*dy**2)
         !ax=-(df_dx(nr_kom,potencjal,n_cell,d_cell)+&
          !     d2f_dx2(nr_kom,potencjal,n_cell,d_cell)*dx+&
          !     d2f_dxdy(nr_kom,potencjal,n_cell,d_cell)*dy)
         !ay=-(df_dy(nr_kom,potencjal,n_cell,d_cell)+&
          !     d2f_dy2(nr_kom,potencjal,n_cell,d_cell)*dy+&
          !     d2f_dxdy(nr_kom,potencjal,n_cell,d_cell)*dx)
   end subroutine interpolacja
   
   function df_dx(nr_kom,potencjal,n_cell,d_cell)
      implicit none
         integer, dimension(1,2), intent(in):: nr_kom
         integer, intent(in):: n_cell
         real, dimension(n_cell,n_cell), intent(in)  :: potencjal
         real, intent(in):: d_cell
         real  ::df_dx, dx,dy
         dx=d_cell
         dy=d_cell
         df_dx=(potencjal(nr_kom(1,1)+1,nr_kom(1,2)) - &
               potencjal(nr_kom(1,1)-1,nr_kom(1,2)))/(2.0*dx)
   end function df_dx
   
   function df_dy(nr_kom,potencjal,n_cell,d_cell)
      implicit none
         integer, dimension(1,2), intent(in):: nr_kom
         integer, intent(in)::n_cell
         real, dimension(n_cell,n_cell), intent(in)  :: potencjal
         real, intent(in):: d_cell
         real  ::df_dy,dy
         dy=d_cell
         df_dy=(potencjal(nr_kom(1,1),nr_kom(1,2)+1) - &
               potencjal(nr_kom(1,1),nr_kom(1,2)-1))/(2.0*dy)
   end function df_dy
   
   function d2f_dx2(nr_kom,potencjal,n_cell,d_cell)
      implicit none
         integer, dimension(1,2), intent(in):: nr_kom
         integer, intent(in)::n_cell
         real, dimension(n_cell,n_cell), intent(in)  :: potencjal
         real, intent(in):: d_cell
         real  ::d2f_dx2, dx,dy
         dx=d_cell
         dy=d_cell
         d2f_dx2=(potencjal(nr_kom(1,1)+1,nr_kom(1,2))-&
               2.0*potencjal(nr_kom(1,1),nr_kom(1,2))+&
               potencjal(nr_kom(1,1)-1,nr_kom(1,2)))/(dx**2)
   end function d2f_dx2
   
   
   function d2f_dy2(nr_kom,potencjal,n_cell,d_cell)
      implicit none
         integer, dimension(1,2), intent(in):: nr_kom
         integer, intent(in)::n_cell
         real, dimension(n_cell,n_cell), intent(in)  :: potencjal
         real, intent(in)::d_cell
         real  ::d2f_dy2, dx,dy
         dx=d_cell
         dy=d_cell
         d2f_dy2=(potencjal(nr_kom(1,1),nr_kom(1,2)+1)-&
               2.0*potencjal(nr_kom(1,1),nr_kom(1,2))+&
               potencjal(nr_kom(1,1),nr_kom(1,2)-1))/(dy**2)
   end function d2f_dy2
   
   
   
   function d2f_dxdy(nr_kom,potencjal,n_cell,d_cell)
      implicit none
         integer, dimension(1,2), intent(in):: nr_kom
         integer, intent(in)::n_cell
         real, dimension(n_cell,n_cell), intent(in)  :: potencjal
         real, intent(in):: d_cell
         real  ::d2f_dxdy, dx,dy
         dx=d_cell
         dy=d_cell
         d2f_dxdy=(potencjal(nr_kom(1,1)+1,nr_kom(1,2)+1) - &
                  potencjal(nr_kom(1,1)+1,nr_kom(1,2)-1) - &
                  potencjal(nr_kom(1,1)-1,nr_kom(1,2)+1) + &
                  potencjal(nr_kom(1,1)-1,nr_kom(1,2)-1))/(4.0*dx*dy)
   end function d2f_dxdy
!koniec programu
end program pm_KDK
