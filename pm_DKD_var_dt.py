#!/usr/bin/env python
# -*- coding : utf -8 -*-
# 
#Program liczacy metoda leapfrog (DKD) ruch ciala w potencjale masy punktowej
#dla zmiennego kroku czasowego

#Autor: A. Skalbania, luty 2014


from numpy import *
from scipy import *
from pylab import *
from array import *


global G
global M
global eta


#////////////      FUNKCJE           /////////////
#----Potencjal masy punktowej
def phi_pm(x,y):
    r = sqrt(x**2+y**2)
    phi_pm = -G*M/(sqrt(r**2+eps**2))
    return phi_pm


#----Pochodne
def der_x(f,x,y,d=1.e-8): 
    der_x = (f(x+d,y) - f(x-d,y))/(2.*d)
    return der_x  
  
def der_y(f,x,y,d=1.e-8):
    der_y = (f(x,y+d) - f(x,y-d))/(2.*d)
    return der_y  

#----Moment pedu L  (przy zalozeniu mp=1)
def mom_pedu(x,y,vx,vy):
    r_dl =  sqrt(x**2+y**2)
    p_dl =  sqrt(vx**2+vy**2)
    mom_pedu   =  r_dl*p_dl*sqrt(1.0-((x*vx+y*vy)/(r_dl*p_dl))**2)
    return mom_pedu


def drift(x,v,t):
    drift   =  x+v*t
    return drift

def kick(v,a,t):
    kick =  v+a*t
    return kick


#---predkosc poczatkowa dla zadanego a (polos wielka) i e (mimosrod)
#---w punkcie startowym znajdujacym sie w apocentrum i przy zalozeniu
#---predkosci poczatkowej Vx0=0.0
def predkosc_poczatkowa(x,y,vx,e,mu):
	r  =  sqrt(x**2+y**2)
	v_kwadrat   =  (2.0*mu*(1.0-e**2))/(r*(r**2.0+(1.0-e**2)))
	vy =  sqrt(v_kwadrat)
	return vy


#---Pozwala wyrysowac co n-ta orbite
def szukaj(t,tablica):
	szukaj   =  tablica.index(t)
	return szukaj



#//////   PROGRAM GLOWNY //////
 
G  = 1.0 
M  = 1.0
mp = 1.0						      #masa czastki probnej


eta   =  30.0						#dokladnosc - wykorzystywane przy obliczaniu zmiennego kroku czasowego
eps   =  1.0e-5

rysuj_orbite_co   =  10.0     #pozwala wyrysowac tylko kilka orbit

e			=	0.9				   #ekscentrycznosc
mu			=	1.0				   #parametr grawitacyjny #G*(M+mp)



#warunki poczatkowe
x0			=	2.0   
y0			=	0.0
vx0		=	0.0
vy0		=	predkosc_poczatkowa(x0,y0,vx0,e,mu)


pol_os   =  x0/2.0            #polos zalezy od punktu startowego

RS       =  2.0*pi*x0/2.0
n_orbit	=	200.0			      #ilosc orbit do pocalkowania

tend		=	n_orbit * RS 	   #czas koncowy

print "Liczba orbit: ", n_orbit



#////// TABLICE /////
t   = array('d',[0.0])
dt  = array('d',[0.0])
x   = array('d',[0.0])
y   = array('d',[0.0])
vx  = array('d',[0.0])
vy  = array('d',[0.0])
E   = array('d',[0.0])                  #Energia calkowita
dE  = array('d',[0.0])
L   = array('d',[0.0])                  #Moment pedu
dL  = array('d',[0.0])
orbity=[0]


# Polozenie poczatkowe
x.append(x0)	                         #x[1]
y.append(y0)	                         #y[1]

#Predkosc poczatkowa
vx.append(vx0)	                         #vx[1]
vy.append(vy0)	                         #vy[1]
print "Polozenie poczatkowe: ",vx[1],vy[1]


E[0]	=	0.5*(vx[1]**2+vy[1]**2)+phi_pm(x[1],y[1])	#Energia poczatkowa
print "Energia poczatkowa: E0=",E[0]


L[0]	=	mom_pedu(x[1],y[1],vx[1],vy[1])				#Poczatkowy moment pedu
print "Moment pedu L0=", L[0]


# Przyspieszenie poczatkowe
ax		=	-der_x(phi_pm,x[1],y[1])
ay		=	-der_y(phi_pm,x[1],y[1])
a		=	sqrt(ax**2+ay**2)

# Zmienny krok czasowy 
dt.append(sqrt(2.0*eta*eps/a))			            #dt[1]
dth	=	dt[1]*0.5




#/////	GLOWNA PETLA PROGRAMU	/////
#-------------------------------------------------------------------------------
n=1
t.append(0.0)
while t[n]<tend:
	#drift-kick-drift
	#
    #1.DRIFT
    xh      =   drift(x[n],vx[n],dth)		
    yh      =   drift(y[n],vy[n],dth)
    #
    #Przyspieszenie
    ax      =   -der_x(phi_pm,xh,yh)
    ay      =   -der_y(phi_pm,xh,yh)
    #
    a       =  sqrt(ax**2+ay**2)
    #
    #2.KICK
    vx.append(kick(vx[n],ax,dt[n]))		   #vx[n+1]
    vy.append(kick(vy[n],ay,dt[n]))		   #vy[n+1]
    #
    #3.DRIFT
    x.append(drift(xh,vx[n+1],dth))       #x[n+1]
    y.append(drift(yh,vy[n+1],dth))       #y[n+1]
    #
    orbity.append(int(floor(t[n]/RS)+1))
    #
    t.append(t[n] + dt[n])				      #t[n+1]
    #
    #zmienny krok calkowania
    dt.append(sqrt(2.0*eta*eps/a))		   #dt[n+1]
    dth=0.5*dt[n+1]
    #
    #
    #
    E.append(0.5*(vx[n]**2+vy[n]**2)+phi_pm(x[n],y[n]))		#E[n]
    dE.append(log(abs((E[n]-E[0])/E[0])))
    #
    L.append(mom_pedu(x[n],y[n],vx[n],vy[n]))				   #L[n]
    dL.append(log(abs((L[n]-L[0])/L[0])))
    n=n+1
    
 
#-------------------------------------------------------------------------------


n_elementow =	n-1	         #liczba wykonanych krokow
n_krokow    =  n_elementow/n_orbit
print str(n_krokow)+" krokow/orbite"


k		=	int(n_orbit/rysuj_orbite_co)





#==========================WYKRESY=========================================
zakres=abs(x0)
#------wykres 1: tor - co n-ta orbita-------------------
figure(figsize=(8,8), dpi=100)

for j in range (1,k):
	t_pocz	=	j*rysuj_orbite_co-1
	t_kon	=	j*rysuj_orbite_co
	pocz	=	szukaj(t_pocz,orbity)
	kon		=	szukaj(t_kon,orbity)
	plot (x[pocz:kon], y[pocz:kon], color ="blue")

text(-1.5,1.7,str(n_orbit)+" orbit")	
text(-1.5,1.55,str(n_krokow)+" krokow/orbite")
text(-1.5,1.4,'$\eta$='+str(eta))
text(-1.5,1.25,'$\epsilon$='+str(eps))

xlim ( -zakres , zakres) 
ylim ( -zakres , zakres) 

xlabel ("$x$ [AU]")
ylabel ("$y$ [AU]") 
title ("Orbita (co "+str(int(rysuj_orbite_co))+")") 
grid()
nazwa	=	"pm_DKD_xy__nstep_"+str(n_krokow)+"_orbit_"+str(n_orbit)+"_eta_"+str(eta)+".png"
savefig(nazwa,dpi=200)


#------wykres 2: blad energii-----------------------
figure(figsize=(8,8), dpi=100)
plot (t[1:n_elementow], dE[1:n_elementow], color ="blue")

xlim ( 0 , tend) 
#ylim ( -1.0 , 1.0) 

xlabel ("$t$")
ylabel (r"$log_{10}\left|\Delta E/E\right|$") 
title (u"Blad Energii") 
grid() 
nazwa="pm_DKD_dE__nstep_"+str(n_krokow)+"_orbit_"+str(n_orbit)+"_eta_"+str(eta)+".png"
savefig(nazwa,dpi=200)


#------wykres 3: blad moment pedu-------------------
figure(figsize=(8,8), dpi=100)
plot (t[1:n_elementow], dL[1:n_elementow], color ="blue")

xlim (0 , tend) 
#ylim ( -1.0 , 1.0) 
 
xlabel ("$t$")
ylabel (r"$log_{10}\left|\Delta L/L\right|$") 
title ("Blad Momentu pedu") 
grid() 
nazwa="pm_DKD_dL__nstep_"+str(n_krokow)+"_orbit_"+str(n_orbit)+"_eta_"+str(eta)+".png"
savefig(nazwa,dpi=200)



#------wykres 4: dt--------------------------------
figure(figsize=(8,8), dpi=100)

plot (t[1:n_elementow], dt[1:n_elementow], linestyle="solid",marker="+", color="blue")

xlim (0 , tend) 
#ylim ( -1.0 , 1.0) 

xlabel ("$t$")
ylabel ("dt") 
title ("Krok calkowania") 
grid() 
nazwa="pm_DKD_dt__nstep_"+str(n_krokow)+"_orbit_"+str(n_orbit)+"_eta_"+str(eta)+".png"
savefig(nazwa,dpi=200)


'''
#------wykres 5: tor - wszystkie orbity-------------------
figure(figsize=(8,8), dpi=100)

plot (x[1:n_elementow],y[1:n_elementow],  linestyle="solid", color="blue")

xlim (-zakres , zakres) 
ylim (-zakres , zakres) 

xlabel ("$x$")
ylabel ("y") 
title ("Polozenie") 
grid() 
nazwa="pm_DKD_xy2__nstep_"+str(n_krokow)+"_orbit_"+str(n_orbit)+"_eta_"+str(eta)+".png"
savefig(nazwa,dpi=200)
'''

show()
