program pm_KDK
	!Program calkujacy potencjal masy punktowej metoda leapfrog KDK przy zmiennym kroku calkowania
	
	!Autor: A. Skalbania, luty 2014
   
   

	implicit none
		integer					:: n, i, orbita
		!zmienne 'dynamiczne'
		real, parameter			:: d=1.0e-6, G=1.0, M=1.0, mp=1.0, pi=3.14159265358979

		!zmienne sterowania krokiem czasowym
		real(kind=8)			:: eta, eps

		!zmienne numeryczne programu
		real(kind=8)			:: RS, n_orbit, tend

		!zmienne orbity poczatkowej
		real(kind=8)			:: x0, y0, vx0, vy0, e, mu, pol_os

		!zmienne
		real(kind=8)			::dth, a, ax, ay
		

		!tablice
		real(kind=8),dimension(1,2)                	::polozenie, predkosc
		real(kind=8)                                 	::t, dt, energy, energia_poczatkowa, denergy, L, dL, L_poczatkowe

		!wektory
		real(kind=8),dimension(1,2)						   ::predkosc_h, tmp


		

		eta	=	35.0				      !dokladnosc - wykorzystywane przy obliczaniu zmiennego kroku czasowego
		eps	=	1.0e-5

		e	=	0.9				      !ekscentrycznosc
		mu	=	1.0				      !parametr grawitacyjny !G*(M+mp)



		!warunki poczatkowe
		x0	=	2.0				      !polozenie poczatkowe
		y0	=	0.0
		vx0	=	0.0				      !predkosc poczatkowa
		vy0	=	predkosc_poczatkowa(x0,y0,vx0,e,mu)
!-------------------------------------------------------------------------------

		pol_os	=	x0/2.0
		
		RS	=	2.0*pi*pol_os		!'czas' jednej orbity
		n_orbit	=	200.0			      !ilosc orbit do pocalkowania

		tend	=	n_orbit * RS 	   !czas koncowy

     
		write(*,'(A23,f12.1)') "Liczba orbit: ", n_orbit



	polozenie(1,1)	=	x0					!polozenie poczatkowe
	polozenie(1,2)	=	y0
		write(*,'(A23,f12.8,A1,f12.8)') "Polozenie poczatkowe: ",polozenie(1,1),",",polozenie(1,2)

	predkosc(1,1)	=	vx0					!predkosc poczatkowa
	predkosc(1,2)	=	vy0
		write(*,'(A23,f12.8,A1,f12.8)') "Predkosc poczatkowa: ",predkosc(1,1),",",predkosc(1,2)


	energia_poczatkowa	=	0.5*(predkosc(1,1)**2 + predkosc(1,2)**2) + phi_pm(polozenie(1,1),polozenie(1,2),eps)
		write(*,'(A23,f12.8)') "Energia poczatkowa: ",energia_poczatkowa

	L_poczatkowe=mom_pedu(polozenie,predkosc)
		write(*,'(A23,f12.8)') "Moment pedu L0: ",L_poczatkowe


	!przyspieszenie poczatkowe
	ax       =  -der_x(polozenie,eps)
	ay       =  -der_y(polozenie,eps)
	a        =  sqrt(ax**2+ay**2)


	! Zmienny krok czasowy 
	dt	=	sqrt(2.0*eta*eps/a)		
	dth	=	dt*0.5

	t	=	0.0
	energy	=	energia_poczatkowa
	denergy	=	0.0
	L	=	L_poczatkowe
        dL	=	0.0

	orbita	=	1


	!Glowna petla programu
	n	=	1
	open(unit=1,file='dane.dat')
	write(1,*) "#      n      t      dt      x     y      vx     vy     energy      denergy     L     dL     orbita"
		do while (t<tend)
			!kick-drift-kick
			write(1,'(i7,10(5X,f16.8),5X,i6)') n,t,dt,polozenie,predkosc,energy,denergy,L,dL,orbita
			!
			!1.KICK
			predkosc_h=	kick(predkosc,ax,ay,dth)
			!
			!2.DRIFT
			polozenie=	drift(polozenie,predkosc_h,dt)
			!
			!Przyspieszenie
			ax	=	-der_x(polozenie,eps)
			ay	=	-der_y(polozenie,eps)
			!
			a	=	sqrt(ax**2+ay**2)
			!  
			!
			!3.KICK
			predkosc=	kick(predkosc_h,ax,ay,dth)
			!
			!
			!
			t	=	t+dt
			dt	=	sqrt(2.0*eta*eps/a)		!dt[n+1]
			dth	=	0.5*dt
			!
			!
			!
			energy	=	0.5*(predkosc(1,1)**2+predkosc(1,2)**2) + phi_pm(polozenie(1,1),polozenie(1,2),eps)
    			denergy	=	log(abs((energy	-	energia_poczatkowa)	/	energia_poczatkowa))
    			L	=	(mom_pedu(polozenie,predkosc))
    			dL	=	log(abs((L-L_poczatkowe)/L_poczatkowe))
    			n	=	n+1
    			orbita	=	int(t/RS)+1
		end do
	close(1)

   write(*,*) "By zobaczyc rezultaty dzialania programu wykonaj polecenie 'gnuplot rysunki.plot'."
   !ewentualnie mozna skorzystac z ponizszej komendy
   !call system("gnuplot rysunki.plot")
	








contains


	!Potencjal masy punktowej
	function phi_pm(x, y, eps)
		real(kind=8)   :: x, y, r, phi_pm, eps
			r		=	sqrt(x**2+y**2)
			phi_pm	=	-G*M/(sqrt(r**2 + eps**2))
	end function phi_pm



	!Pochodna wzgledem x
	function der_x(polozenie,d)
		real(kind=8)   :: f, x, y, der_x, d
		real(kind=8), dimension(1,2)   :: polozenie
			x	=	polozenie(1,1)
			y	=	polozenie(1,2)
			der_x	= 	(phi_pm(x+d,y,eps) - phi_pm(x-d,y,eps)) / (2.0*d)
	end function der_x



	!Pochodna wzgledem x
	function der_y(polozenie,d)
		real(kind=8)   :: f, x, y, der_y, d
		real(kind=8), dimension(1,2)   :: polozenie
			x	=	polozenie(1,1)
			y	=	polozenie(1,2)
			der_y	=	(phi_pm(x,y+d,eps) - phi_pm(x,y-d,eps)) / (2.0*d)
	end function der_y



	!Moment pedu
	function mom_pedu(polozenie, predkosc)
		real(kind=8)   :: mom_pedu, r_dl, p_dl, x, y, vx, vy
		real(kind=8), dimension(1,2)  :: polozenie, predkosc
         		x	=	polozenie(1,1)
         		y	=	polozenie(1,2)
         		vx	=	predkosc(1,1)
         		vy	=	predkosc(1,2)	
         		r_dl	=	sqrt(x**2+y**2)
         		p_dl	=	sqrt(vx**2+vy**2)
         		mom_pedu=	r_dl*p_dl*sqrt(1.0-((x*vx+y*vy) / (r_dl*p_dl))**2)
	end function mom_pedu



	!Drift
	function drift(polozenie,predkosc,t)
		real(kind=8)   :: x, y, vx, vy, t
		real(kind=8), dimension(1,2)  :: drift, polozenie, predkosc
			x		=	polozenie(1,1)
			y		=	polozenie(1,2)
			vx		=	predkosc(1,1)
			vy		=	predkosc(1,2)
			drift(1,1)	=	x+vx*t
			drift(1,2)	=	y+vy*t
	end function drift



	!Kick
	function kick(predkosc, ax, ay, t)
		real(kind=8)   :: vx, vy, ax, ay, t
		real(kind=8), dimension(1,2)  :: kick, predkosc
			vx		=	predkosc(1,1)
			vy		=	predkosc(1,2)
			kick(1,1)	=	vx+ax*t
			kick(1,2)	=	vy+ay*t
	end function kick



	!predkosc poczatkowa dla zadanego a (polos wielka) i e (mimosrod)
	!w punkcie startowym znajdujacym sie w apocentrum i przy zalozeniu
	!predkosci poczatkowej Vx0=0.0
	function predkosc_poczatkowa(x, y, vx, e, mu)
		real(kind=8)   :: x, y, vx, e, mu, r, v2, predkosc_poczatkowa
			r	=	sqrt(x**2+y**2)
			v2	=	((2.0*mu*(1-e**2))/(r*(r**2+(1-e**2))))
			predkosc_poczatkowa	=	sqrt(v2)
	end function predkosc_poczatkowa

!koniec programu
end program pm_KDK
